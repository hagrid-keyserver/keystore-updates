#!/usr/bin/make -f

draft = keystore-updates
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf

all: $(OUTPUT)

%.xmlv2: %.md
	kramdown-rfc2629 < $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	xml2rfc --v2v3 -o $@ $<

%.html: %.xml
	xml2rfc $< --html

%.pdf: %.xml
	xml2rfc $< --pdf

%.txt: %.xml
	xml2rfc $< --text

clean:
	-rm -rf $(OUTPUT) $(draft).xmlv2

.PHONY: clean all
.SECONDARY: $(draft).xmlv2
